package itech.resources;

import itech.helloWorldService.Gender;
import itech.helloWorldService.HelloWorld;
import org.glassfish.jersey.client.JerseyClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("helloWorld")
public class HelloWorldResource extends JerseyClient {

    @Path("/simpleTextString")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String helloWorldString() {
        String helloWorld = HelloWorld.helloWorldString();
        return helloWorld;
    }

    @Path("/simpleTextAccepted")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloWorldAccepted() {
        String helloWorld = HelloWorld.helloWorldString();
        return Response.accepted(helloWorld).build();
    }

    @Path("/simpleTextNoContent")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloWorldNoContent() {
        String helloWorld = HelloWorld.helloWorldString();
        return Response.noContent().build();
    }

    @Path("/simpleTextOk")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloWorld() {
        String helloWorld = HelloWorld.helloWorldString();
        return Response.ok(helloWorld, MediaType.TEXT_PLAIN).build();
    }

    @Path("/simpleTextGreetings")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloWorldGreetings(
            @QueryParam("name")String name,
            @QueryParam("gender") Gender gender) {
        String helloWorldGreetings = HelloWorld.helloWorldStringAdvanced(name, gender);
        return Response.ok(helloWorldGreetings, MediaType.TEXT_PLAIN).build();
    }

}
